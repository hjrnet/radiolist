package com.hjrnet.radio;

import android.content.Context;
import android.content.Intent;

/**
 * Created by hjrnet on 25/02/17.
 */

public class IntentReceiver extends android.content.BroadcastReceiver {

    public void onReceive(Context ctx, Intent intent) {
        if (intent.getAction().equals(
                android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY)) {
            Intent myIntent = new Intent(ctx, StreamService.class);
            ctx.stopService(myIntent);
        }
    }
}