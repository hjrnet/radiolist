package com.hjrnet.radio;

import java.io.IOException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.TextView;

public class StreamService extends Service{
	private static final String TAG = "StreamService";
	public MediaPlayer  mp = new MediaPlayer();;

//	@Nullable
//	@Override
//	public IBinder onBind(Intent intent) {
//		return null;
//	}

	boolean isPlaying;
	SharedPreferences prefs;
	SharedPreferences.Editor editor;
	Notification n;
	NotificationManager notificationManager;
	//public String url = "http://176.31.115.196:8214/";
	// Change this int to some number specifically for this app
	MainActivity ma = new MainActivity();
	int notifId = 5315;
//	MainActivity.MyTimerTask myTask = new MainActivity.MyTimerTask();
	Timer myTimer = new Timer();
	MyTimerTask myTask = new MyTimerTask();
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate");

		// Init the SharedPreferences and Editor
		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		editor = prefs.edit();

//
//		notificationManager = new NotificationCompat.Builder(this)
//				.addAction(android.R.drawable.ic_media_play, "Play",
//						pplayIntent)
//				.addAction(android.R.drawable.ic_media_next, "Next",
//						pnextIntent).build();
		
		// Set up the buffering notification
		notificationManager = (NotificationManager) getApplicationContext()
				.getSystemService(NOTIFICATION_SERVICE);
		myTimer.schedule(myTask, 0, 1000);
		Context context = getApplicationContext();
		
		String notifTitle = context.getResources().getString(R.string.app_name);
		String notifMessage = context.getResources().getString(R.string.buffering);
		
		n = new Notification();
		n.icon = R.drawable.ic_launcher;
		n.tickerText = "Buffering";
		n.when = System.currentTimeMillis();

		Intent nIntent = new Intent(context, MainActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, nIntent, 0);

		IntentFilter screenStateFilter = new IntentFilter();
		screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
		screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);

//        BroadcastReceiver mScreenStateReceiver;
//        registerReceiver(nIntent, screenStateFilter);


		n.setLatestEventInfo(context, notifTitle, notifMessage, pIntent);
		
		notificationManager.notify(notifId, n);




		// It's very important that you put the IP/URL of your ShoutCast stream here
		// Otherwise you'll get Webcom Radio


	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		Bundle bundle = intent.getExtras();
        if(intent != null){



//            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);


//            try {
//                mp.setDataSource( bundle.getString("url"));
//				mp.prepareAsync();
//                mp.prepare();
//                URL url = new URL( bundle.getString("link"));
//                ParsingHeaderData streaming = new ParsingHeaderData();
//                ParsingHeaderData.TrackData trackData = streaming.getTrackDetails(url);
//                Log.e("Song Artist Name ", trackData.artist);
//                Log.e("Song Artist Title", trackData.title);
//                System.out.println("Song Artist Name "+ trackData.artist);

//            } catch (IllegalArgumentException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            } catch (SecurityException e) {
//                // TODO Auto-generated catch block
//                Log.e(TAG, "SecurityException");
//            } catch (IllegalStateException e) {
//                // TODO Auto-generated catch block
//                Log.e(TAG, "IllegalStateException");
//            } catch (IOException e) {
//                // TODO Auto-generated catch block
//                Log.e(TAG, "IOException");
//            }

        }


		Log.d(TAG, "onStart");
//		mp.start();
		// Set the isPlaying preference to true
		editor.putBoolean("isPlaying", true);
		editor.commit();

		Context context = getApplicationContext();
		String notifTitle = context.getResources().getString(R.string.app_name);
		String notifMessage =  bundle.getString("nome");

		n.icon = R.drawable.ic_launcher;
		n.tickerText = notifMessage;
		n.flags = Notification.FLAG_NO_CLEAR;
		n.when = System.currentTimeMillis();
		ma.btPlay=false;

		Intent nIntent = new Intent(context, MainActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, nIntent, 0);

		n.setLatestEventInfo(context, notifTitle, notifMessage, pIntent);
		// Change 5315 to some nother number
		notificationManager.notify(notifId, n);
	}




	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		mp.stop();
		mp.release();
		mp = null;
		editor.putBoolean("isPlaying", false);
		editor.commit();
		notificationManager.cancel(notifId);
	}

}

class MyTimerTask extends TimerTask {
	public void run() {
//		this.mp.start();
		// ERROR
//			hTextView.setText("Impossible");
		// how update TextView in link below
		// http://android.okhelp.cz/timer-task-timertask-run-cancel-android-example/
		//	startButton.setEnabled(btPlay);

//			System.out.println(radio);
	}
}