package com.hjrnet.radio;;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends Activity {
	// Declare Variables
	Button  bt1, bt2,bt3, bt4, bt5;
	ConnectivityManager connectivityManager;
	NetworkInfo wifiInfo, mobileInfo;
	static Context context;
	boolean isPlaying;
	private ImageButton socialshared;
	public static boolean btPlay;
	public static String generoEscolhido="DEEP";
	public static String enderecoJson="http://www.adliix.com/EMUSIC/" ;
//	public static String enderecoJson="http://10.0.0.124/" ;

	private static MainActivity   _instance;
//	Intent streamService;
	SharedPreferences prefs;
	JSONObject jsonobject;
	JSONArray jsonarray;
	ListView listview;
	ListViewAdapter adapter;
	ProgressDialog mProgressDialog;
	ArrayList<HashMap<String, String>> arraylist;

	public static String jsonUrl;




	public static String radio="Compartilhe / Share";
	public static String linkradio="teste";
	static String NOME = "nome";
	static String ESTILO = "estilo";
	static String DESCRICAO = "descricao";
	static String URL = "url";
	static String FLAG = "flag";
	Intent intent;
	public TextView nomeradio;

	//public static String generoEscolhido;

//	SingleItemView singleItemView = new SingleItemView();

	MyTimerTask myTask = new MyTimerTask();
	Timer myTimer = new Timer();

	public synchronized static MainActivity getInstance()
	{
		if (_instance == null)
		{
			_instance = new MainActivity();
		}
		return _instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from listview_main.xml
		setContentView(R.layout.listview_main);
//
//		getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
//				android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

//		AdView adView = (AdView) this.findViewById(R.id.adMob);
////		adView.setAdUnitId("ca-app-pub-1687078348394051/1536823321");
//		//request TEST ads to avoid being disabled for clicking your own ads
//		AdRequest adRequest = new AdRequest.Builder()
////				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)// This is for emulators
////				//test mode on DEVICE (this example code must be replaced with your device uniquq ID)
////				.addTestDevice("C91DADC3C3B0DAC40F7965B35DDE6D84") // Nexus 5
//
//				.build();
//		adView.loadAd(adRequest);




		AdView mAdView = (AdView) findViewById(R.id.adMob);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
//

//		AdView adView =  (AdView) this.findViewById(R.id.adMob);
//		adView.setAdSize(AdSize.SMART_BANNER);
//		adView.setAdUnitId("ca-app-pub-1687078348394051/1536823321");
//		// Request for Ads
//		AdRequest adRequest = new AdRequest.Builder().build();
//
//		// Load ads into Banner Ads
//		adView.loadAd(adRequest);
		this.socialshared = (ImageButton) findViewById(R.id.socialshared2);
		bt1 = (Button) findViewById(R.id.DEEP);
		bt4=(Button) findViewById(R.id.TRANCE);
		bt2=(Button) findViewById(R.id.ELECTRO);
		bt3=(Button) findViewById(R.id.TECHNO);
		bt5=(Button) findViewById(R.id.AMBIENT);
//		setContentView(R.layout.singleitemview);
		// Execute DownloadJSON AsyncTask
		if(generoEscolhido=="DEEP"){
			bt1.setEnabled(false);
			jsonUrl=enderecoJson+"DEEP.txt";


			new DownloadJSON().execute();

		}

		else if(generoEscolhido=="ELECTRO"){
			bt2.setEnabled(false);
			jsonUrl=enderecoJson+"ELECTRO.txt";

			new DownloadJSON().execute();

		}
		else if(generoEscolhido=="TECHNO"){
			bt3.setEnabled(false);
			jsonUrl=enderecoJson+"TECHNO.txt";

			new DownloadJSON().execute();

		}
		else if(generoEscolhido=="AMBIENT"){
			bt3.setEnabled(false);
			jsonUrl=enderecoJson+"AMBIENT.txt";

			new DownloadJSON().execute();

		}


		myTimer.schedule(myTask, 0, 1000);
//		startButton.setEnabled(isPlaying);

		context = this;
//		startButton = (Button) findViewById(R.id.startButton);
//		stopButton = (Button) findViewById(R.id.stopButton2);




//		startButton2 = (Button) findViewById(R.id.startButton2);
//		stopButton2 = (Button) findViewById(R.id.stopButton2);

		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		getPrefs();
//		btPlay=false;
//		startButton.setEnabled(btPlay);
//		streamService = new Intent(MainActivity.this, StreamService.class);
		 intent = new Intent(context, SingleItemView.class);
		socialshared.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent sendIntent = new Intent();
				sendIntent.setAction(Intent.ACTION_SEND);
				sendIntent.putExtra(Intent.EXTRA_TEXT,
						"Ei veja esse app de música eletrônica. \n\nHey check out app electronic music.\n\nhttps://play.google.com/store/apps/details?id=com.hjrnet.radio");
				sendIntent.setType("text/plain");
				startActivity(sendIntent);
			}
		});




//		stopButton.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
////				stopService(streamService);
//
//				btPlay=true;
//				startButton.setEnabled(btPlay);
//				//isPlaying=true;
//
//			}
//		});
//
//		startButton.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Bundle b = new Bundle();
//				b.putString("url", linkradio);
//				b.putString("nome", radio);
////				streamService.putExtras(b);
//
////				startService(streamService);
//				btPlay=false;
//				startButton.setEnabled(btPlay);
//			}
//		});
		bt1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bt1.setEnabled(false);
				bt4.setEnabled(true);
				bt2.setEnabled(true);
				bt3.setEnabled(true);
				bt5.setEnabled(true);

				generoEscolhido="DEEP";
				jsonUrl=enderecoJson+"DEEP.txt";
				new DownloadJSON().execute();


			}
		});
		bt2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				generoEscolhido="ELECTRO";
				// TODO Auto-generated method stub
				bt1.setEnabled(true);
				bt4.setEnabled(true);
				bt2.setEnabled(false);
				bt3.setEnabled(true);
				bt5.setEnabled(true);
				jsonUrl=enderecoJson+"ELECTRO.txt";
				new DownloadJSON().execute();
			}
		});

		bt3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bt1.setEnabled(true);
				bt4.setEnabled(true);
				bt2.setEnabled(true);
				bt3.setEnabled(false);
				bt5.setEnabled(true);
				generoEscolhido="TECHNO";
				jsonUrl=enderecoJson+"TECHNO.txt";
				new DownloadJSON().execute();
				//isPlaying=true;
			}
		});
		bt4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bt1.setEnabled(true);
				bt4.setEnabled(false);
				bt2.setEnabled(true);
				bt3.setEnabled(true);
				bt5.setEnabled(true);
				generoEscolhido="TRANCE";
				jsonUrl=enderecoJson+"TRANCE.txt";
				new DownloadJSON().execute();
				//isPlaying=true;
			}
		});

		bt5.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bt1.setEnabled(true);
				bt4.setEnabled(true);
				bt2.setEnabled(true);
				bt3.setEnabled(true);
				bt5.setEnabled(false);
				generoEscolhido="AMBIENT";
				jsonUrl=enderecoJson+"AMBIENT.txt";
				new DownloadJSON().execute();
				//isPlaying=true;
			}
		});


//
//		stopButton2.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				stopService(streamService);
//				startButton2.setEnabled(true);
//			}
//		});


	}

	public void getPrefs() {
		isPlaying = prefs.getBoolean("isPlaying", false);
//		if (isPlaying) startButton.setEnabled(false);
	}

	public void Teste() {
		//Log.d("Meu pau =======", "clicou ");
//	startButton.setEnabled(btPlay);
	}

	public boolean isInternetAvailable() {
		try{
			connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			if(wifiInfo.isConnected() || mobileInfo.isConnected())
			{
				return true;
			}
		}
		catch(Exception e){
			System.out.println("CheckConnectivity Exception: " + e.getMessage());
		}

		return false;


}
	// DownloadJSON AsyncTask
	private class DownloadJSON extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();


				mProgressDialog = new ProgressDialog(MainActivity.this);
				// Set progressdialog title
				mProgressDialog.setTitle("E-Music Radio");
				// Set progressdialog message
				mProgressDialog.setMessage("Loading...");
				mProgressDialog.setIndeterminate(false);
				// Show progressdialog
				mProgressDialog.show();

		}

		@Override
		protected Void doInBackground(Void... params) {
			// Create an array

				arraylist = new ArrayList<HashMap<String, String>>();
				// Retrieve JSON Objects from the given URL address
				jsonobject = JSONfunctions
						.getJSONfromURL(jsonUrl);
				try {
					// Locate the array name in JSON
					jsonarray = jsonobject.getJSONArray("webradios");

					for (int i = 0; i < jsonarray.length(); i++) {
						HashMap<String, String> map = new HashMap<String, String>();
						jsonobject = jsonarray.getJSONObject(i);
						// Retrive JSON Objects

						map.put("nome", jsonobject.getString("nome"));
						map.put("descricao", jsonobject.getString("descricao"));
						map.put("estilo", jsonobject.getString("estilo"));
						map.put("url", jsonobject.getString("url"));
						map.put("flag", jsonobject.getString("flag"));
						// Set the JSON Objects into the array
						arraylist.add(map);
					}
				} catch (JSONException e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}

			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

				// Locate the listview in listview_main.xml
				listview = (ListView) findViewById(R.id.listview);
				// Pass the results into ListViewAdapter.java
				adapter = new ListViewAdapter(MainActivity.this, arraylist);
				// Set the adapter to the ListView
				listview.setAdapter(adapter);
				// Close the progressdialog
				mProgressDialog.dismiss();


		}


	}




	class MyTimerTask extends TimerTask {
		public void run() {
			// ERROR
//			hTextView.setText("Impossible");
			// how update TextView in link below
			// http://android.okhelp.cz/timer-task-timertask-run-cancel-android-example/
		//	startButton.setEnabled(btPlay);
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					nomeradio = (TextView) findViewById(R.id.nomeradio);
					if(radio=="Compartilhe / Share"){
						nomeradio.setText(radio);
					}else{
						nomeradio.setText("Última escolhida: - "+radio);
					}

//					startButton.setEnabled(btPlay);
//					startService(streamService);
				}
			});
//			System.out.println(radio);
		}
	}




}
