package com.hjrnet.radio;;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.IOException;

public class SingleItemView extends Activity {

	private MediaPlayer player;						// Media player
//	private MetaTask metaTask;						// Async Task for continuous updating
//	private ImageView logo;							// Company logo
//	private TextView artist, title, serverTitle, timetv;	// Artist & Title data
//	private ImageButton togglePlay;					// Play/Stop button
//	private TableLayout scheduleTable;
	//    private NotificationCompat.Builder notificationBuilder = null;
//	private NotificationManager mNotificationManager;
	private int NotificationId = 123454321;
	private boolean isPlaying;
	private boolean isPlayerLoaded;
//	private Time t = new Time();
	private String day = "Splorgday";
	private String scheduleJSON = "";

	Chronometer chronometer;
	// Declare Variables
	Button startButton, stopButton, startButton2, stopButton2;
	private ImageButton togglePlay;
	private ImageButton socialshared;
	String nome;
	String descricao;
	String url;
	String estilo;
	String flag;
	String position;
	String buffer;
	ImageLoader imageLoader = new ImageLoader(this);
	boolean isPlaying2;
	MainActivity ma = new MainActivity();
	public TextView txtbuffer;
//	Intent streamService;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);


		this.player			= new MediaPlayer();
//		this.player.stop();
//		this.player.reset();
//		player.stop();
		// Get the view from singleitemview.xml
		setContentView(R.layout.singleitemview);
//		getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
//				android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		AdView adView = (AdView) this.findViewById(R.id.adMob);

		//request TEST ads to avoid being disabled for clicking your own ads
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)// This is for emulators
//				//test mode on DEVICE (this example code must be replaced with your device uniquq ID)
//				.addTestDevice("C91DADC3C3B0DAC40F7965B35DDE6D84") // Nexus 5
				.build();
		adView.loadAd(adRequest);
//		setContentView(R.layout.listview_main);
//		startButton2 = (Button) findViewById(R.id.startButton2);
//		stopButton2 = (Button) findViewById(R.id.stopButton2);
		this.togglePlay		= (ImageButton) findViewById(R.id.togglePlay);
		this.socialshared = (ImageButton) findViewById(R.id.socialshared);
//		startButton = (Button) findViewById(R.id.startButton);
//		stopButton = (Button) findViewById(R.id.stopButton);
//		streamService = new Intent(SingleItemView.this, StreamService.class);
		txtbuffer = (TextView) findViewById(R.id.buffer);
		txtbuffer.setText("Selected Radio");
		this.player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			public void onPrepared(MediaPlayer mp) {
//				mp.stop();
//				mp.reset();
				mp.start();

				SingleItemView.this.togglePlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.stop));

				int topContainerId = getResources().getIdentifier("mediacontroller_progress", "id", "android");
//				final ProgressBar seekbar = (ProgressBar) findViewById(R.id.progressBar);
				System.out.println("bufering1 "+ topContainerId);
//				txtbuffer.setText("Lendo...");
				if(txtbuffer.getText()=="Lendo..."){
					txtbuffer.setText("Tocando");
				}
				chronometer.stop();
				chronometer.start();
				mp.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
					@Override
					public void onBufferingUpdate(MediaPlayer mp, int percent) {
//							if (percent<seekbar.getMax()) {
//						seekbar.setSecondaryProgress(percent);
//						seekbar.setSecondaryProgress(percent/100);
						System.out.println("bufering2 "+percent/-100);
//						txtbuffer.setText("Tocando...");
						chronometer.start();
						txtbuffer.setText("Tocando");

//							}

					}
				});
			}

		});


		this.togglePlay.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				togglePlay();
			}
		});

		Intent i = getIntent();
		// Get the result of rank
		nome = i.getStringExtra("nome");
		// Get the result of country
		descricao = i.getStringExtra("descricao");
		// Get the result of population
		estilo = i.getStringExtra("estilo");
		// Get the result of flag
		flag = i.getStringExtra("flag");

		url  = i.getStringExtra("url");
		// Locate the TextViews in singleitemview.xml
		TextView txtnome = (TextView) findViewById(R.id.nome);
		TextView txtdescricao = (TextView) findViewById(R.id.descricao);
		TextView txtestilo = (TextView) findViewById(R.id.estilo);

		chronometer = (Chronometer) findViewById(R.id.chronometer);
		// Locate the ImageView in singleitemview.xml
		ImageView imgflag = (ImageView) findViewById(R.id.flag);

		socialshared.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent sendIntent = new Intent();
				sendIntent.setAction(Intent.ACTION_SEND);
				sendIntent.putExtra(Intent.EXTRA_TEXT,
						"Ei veja esse app de música eletrônica. \n\nHey check out app electronic music.\n\nhttps://play.google.com/store/apps/details?id=com.hjrnet.radio");
				sendIntent.setType("text/plain");
				startActivity(sendIntent);
			}
		});
		// Set results to the TextViews
		txtnome.setText(nome);
		txtdescricao.setText(descricao);
		txtestilo.setText(estilo);

		Bundle b = new Bundle();
		b.putString("url", url);
		b.putString("nome", nome);


		togglePlay();
		chronometer.setBase(SystemClock.elapsedRealtime());
//		chronometer.start();
		if(nome.equals(ma.radio) ){

        }else{
//            streamService.putExtras(b);

            ma.radio=nome;
            ma.linkradio=url;

//            stopService(streamService);
//            startService(streamService);
            ma.btPlay = false;
        }


//		if(ma.btPlay == true) {

//		};
		// Capture position and set results to the ImageView
		// Passes flag images URL into ImageLoader.class
		imageLoader.DisplayImage(flag, imgflag);

//		startButton2.setEnabled(ma.btPlay);

//		startButton2.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Bundle b = new Bundle();
//				b.putString("url", url);
//				b.putString("nome", nome);
////				streamService.putExtras(b);
//
////				startService(streamService);
//				ma.btPlay=false;
//				startButton2.setEnabled(ma.btPlay);
////				ma.startButton.setEnabled(ma.btPlay);
//			}
//		});
//
//		stopButton2.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
////				stopService(streamService);
//				ma.btPlay=true;
//				startButton2.setEnabled(ma.btPlay);
////				ma.startButton.setEnabled(ma.btPlay);
////				ma.startButton.setEnabled(true);
//				togglePlay();
////				ma.stop();
////				ma.getPrefs();
//				//ma.getPrefs();
//			}
//		});


	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK ) {
//			startButton.setEnabled(ma.btPlay);
			player.reset();
			player.stop();
			chronometer.stop();
			chronometer.setBase(SystemClock.elapsedRealtime());
		}
		return super.onKeyDown(keyCode, event);
	}

	private void togglePlay() {
		try {
			if (!this.player.isPlaying()) {
				isPlaying = true;
				txtbuffer.setText("Lendo...");
				this.player.setAudioStreamType(AudioManager.STREAM_MUSIC);
				this.player.setDataSource(url);
				this.player.prepareAsync();
				isPlayerLoaded = true;
				this.player.start();
				chronometer.stop();
				SingleItemView.this.togglePlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.stop));
//				UpdateNotification();
			} else {
				txtbuffer.setText("Parado");
				isPlaying = false;
				this.player.reset();
				chronometer.stop();
				chronometer.setBase(SystemClock.elapsedRealtime());

				this.togglePlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.play));
//				ClearNotification();
			}
		} catch (IllegalArgumentException e) {
			Log.e("ERROR: togglePlay", "Invalid data source!");
			e.printStackTrace();
		} catch (IllegalStateException e) {
			Log.e("ERROR: togglePlay", "Play state buggered up!");
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("ERROR: togglePlay", "Invalid data source!");
			e.printStackTrace();
		}
	}



}